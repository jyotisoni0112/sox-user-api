package com.sox.users.utility;

import com.sox.users.entity.UserRoleDetails;
import com.sox.users.payload.UserResponse;

import java.util.ArrayList;
import java.util.List;

public class UtilityMethod {

    public static String capitalize(String string) {
        if(string == null )
            return "";
        else{
            if(string.length()>1){
                return string.substring(0,1).toUpperCase()+string.substring(1);
            }else{
                return string.toUpperCase();
            }
        }
    }

    public static List<UserResponse> mapRoleResponseToUserResponse(List<UserRoleDetails> roleDetails){
        List<UserResponse> response = new ArrayList<>();
        roleDetails.forEach(user -> {
            UserResponse userResponse = new UserResponse();
            userResponse.setUserRoleId(user.getUserRoleId());
            userResponse.setUserId(user.getUserId());
            userResponse.setRoleId(user.getRoleId());
            userResponse.setRoleName(RoleUtility.getRoleName(user.getRoleId()));
            userResponse.setAppId(user.getAppId());
            if(user.getApplication() == null){
                userResponse.setAppName("");
                userResponse.setLocName("");
                userResponse.setEmail("");
                userResponse.setFirstName("");
                userResponse.setLastName("");
            }else{
                userResponse.setAppName(user.getApplication().getAppName());
                userResponse.setLocName(user.getLocation().getLocName());
                userResponse.setEmail(user.getUserInfo().getEmail());
                userResponse.setFirstName(user.getUserInfo().getFirstName());
                userResponse.setLastName(user.getUserInfo().getLastName());
            }
            userResponse.setLocId(user.getLocId());
            userResponse.setActive(user.isActive());
            response.add(userResponse);
        });
        return response;
    }

    private UtilityMethod(){

    }

}
