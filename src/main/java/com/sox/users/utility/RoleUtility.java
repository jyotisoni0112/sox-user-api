package com.sox.users.utility;

public class RoleUtility {
    public static final int ROLE_ADMIN = 9;
    public static final int ROLE_DASHBOARD_ADMIN = 10;
    public static final int ROLE_BUSINESS_POC_1 = 1;
    public static final int ROLE_BUSINESS_B_POC_1 = 2;
    public static final int ROLE_BUSINESS_POC_2 = 3;
    public static final int ROLE_BUSINESS_B_POC_2 = 4;
    public static final int ROLE_BUSINESS_POC_3 = 5;
    public static final int ROLE_BUSINESS_B_POC_3 = 6;
    public static final int ROLE_READ_ONLY = 7;
    public static final int ROLE_IT_OWNER = 12;
    public static final int ROLE_BUSINESS_OWNER = 13;
    public static final int ROLE_AUDITOR_DELOITTE = 14;
    public static final int ROLE_MJE_ADMIN = 15;
    public static final int ROLE_MJE_REVIEWER = 16;
    public static final int ROLE_PAC_ADMIN = 17;
    public static final int ROLE_PAC_REVIEWER = 18;
    public static final int ROLE_USER_STATUS_ADMIN = 19;
    public static final int ROLE_USER_STATUS_USER = 20;
    public static final int ROLE_AUDITOR_EY = 21;
    public static final int ROLE_GCA_SOX_CHAMP = 22;
    public static final int ROLE_GCA_USERS = 23;
    public static final int ROLE_NO_ROLE = 0;

    public static final String ROLE_NAME_ADMIN = "Admin";
    public static final String ROLE_NAME_DASHBOARD_ADMIN = "Dashboard Admin";
    public static final String ROLE_NAME_BUSINESS_POC_1 = "Business Access Reviewer 1";
    public static final String ROLE_NAME_BUSINESS_B_POC_1 = "Business Access Backup Reviewer 1";
    public static final String ROLE_NAME_BUSINESS_POC_2 = "Business Access Reviewer 2";
    public static final String ROLE_NAME_BUSINESS_B_POC_2 = "Business Access Backup Reviewer 2";
    public static final String ROLE_NAME_BUSINESS_POC_3 = "Business Access Reviewer 3";
    public static final String ROLE_NAME_BUSINESS_B_POC_3 = "Business Access Backup Reviewer 3";
    public static final String ROLE_NAME_READ_ONLY = "Read Only";
    public static final String ROLE_NAME_IT_OWNER = "IT Owner";
    public static final String ROLE_NAME_BUSINESS_OWNER = "Business Owner";
    public static final String ROLE_NAME_AUDITOR_DELOITTE = "Auditor - Delloite";
    public static final String ROLE_NAME_MJE_ADMIN = "MJE Admin";
    public static final String ROLE_NAME_MJE_REVIEWER = "MJE Reviewer";
    public static final String ROLE_NAME_PAC_ADMIN = "PAC Admin";
    public static final String ROLE_NAME_PAC_REVIEWER = "PAC Reviewer";
    public static final String ROLE_NAME_USER_STATUS_ADMIN = "User Status Admin";
    public static final String ROLE_NAME_USER_STATUS_USER = "User Status";
    public static final String ROLE_NAME_AUDITOR_EY = "Auditor - EY";
    public static final String ROLE_NAME_GCA_SOX_CHAMP = "GAC Champ";
    public static final String ROLE_NAME_GCA_USERS = "GCA User";
    public static final String ROLE_NAME_NO_ROLE = "No Role";

    public static String getRoleName(int roleId){
        switch (roleId){
            case ROLE_BUSINESS_POC_1: return ROLE_NAME_BUSINESS_POC_1;
            case ROLE_BUSINESS_B_POC_1: return ROLE_NAME_BUSINESS_B_POC_1;
            case ROLE_BUSINESS_POC_2: return ROLE_NAME_BUSINESS_POC_2;
            case ROLE_BUSINESS_B_POC_2: return ROLE_NAME_BUSINESS_B_POC_2;
            case ROLE_BUSINESS_POC_3: return ROLE_NAME_BUSINESS_POC_3;
            case ROLE_BUSINESS_B_POC_3: return ROLE_NAME_BUSINESS_B_POC_3;
            case ROLE_READ_ONLY: return ROLE_NAME_READ_ONLY;
            case ROLE_ADMIN: return ROLE_NAME_ADMIN;
            case ROLE_DASHBOARD_ADMIN: return ROLE_NAME_DASHBOARD_ADMIN;
            case ROLE_IT_OWNER: return ROLE_NAME_IT_OWNER;
            case ROLE_BUSINESS_OWNER: return ROLE_NAME_BUSINESS_OWNER;
            case ROLE_AUDITOR_DELOITTE: return ROLE_NAME_AUDITOR_DELOITTE;
            case ROLE_MJE_ADMIN: return ROLE_NAME_MJE_ADMIN;
            case ROLE_MJE_REVIEWER: return ROLE_NAME_MJE_REVIEWER;
            case ROLE_PAC_ADMIN: return ROLE_NAME_PAC_ADMIN;
            case ROLE_PAC_REVIEWER: return ROLE_NAME_PAC_REVIEWER;
            case ROLE_USER_STATUS_ADMIN: return ROLE_NAME_USER_STATUS_ADMIN;
            case ROLE_USER_STATUS_USER: return ROLE_NAME_USER_STATUS_USER;
            case ROLE_AUDITOR_EY: return ROLE_NAME_AUDITOR_EY;
            case ROLE_GCA_SOX_CHAMP: return ROLE_NAME_GCA_SOX_CHAMP;
            case ROLE_GCA_USERS: return ROLE_NAME_GCA_USERS;
            case ROLE_NO_ROLE: return ROLE_NAME_NO_ROLE;
            default:return null;
        }
    }

    protected static final int[] ROLE_LIST_DASHBOARD = {ROLE_ADMIN, ROLE_DASHBOARD_ADMIN, ROLE_IT_OWNER, ROLE_BUSINESS_OWNER, ROLE_AUDITOR_DELOITTE,
            ROLE_AUDITOR_EY, ROLE_GCA_SOX_CHAMP, ROLE_GCA_USERS};

    protected static final int[] ROLE_LIST_UAR = {ROLE_ADMIN, ROLE_BUSINESS_POC_1, ROLE_BUSINESS_B_POC_1, ROLE_BUSINESS_POC_2, ROLE_BUSINESS_B_POC_2
            , ROLE_BUSINESS_POC_3, ROLE_BUSINESS_B_POC_3, ROLE_READ_ONLY};

    protected static final int[] ROLE_LIST_MJE = {ROLE_ADMIN, ROLE_MJE_ADMIN, ROLE_MJE_REVIEWER};

    protected static final int[] ROLE_LIST_PAC = {ROLE_ADMIN, ROLE_PAC_ADMIN, ROLE_PAC_REVIEWER};

    protected static final int[] ROLE_LIST_USER_STATUS = {ROLE_ADMIN, ROLE_USER_STATUS_ADMIN, ROLE_USER_STATUS_USER};

    protected static final int[] ROLE_LIST_ADMIN = {ROLE_ADMIN};

    public static int[] roleMapping(String module){
        switch (module){
            case "DASHBOARD": return ROLE_LIST_DASHBOARD;
            case "UAR": return ROLE_LIST_UAR;
            case "MJE": return ROLE_LIST_MJE;
            case "PAC": return ROLE_LIST_PAC;
            case "USER_STATUS": return ROLE_LIST_USER_STATUS;
            case "ADMIN": return ROLE_LIST_ADMIN;
            default:return new int[]{};
        }
    }

    private RoleUtility(){}
}
