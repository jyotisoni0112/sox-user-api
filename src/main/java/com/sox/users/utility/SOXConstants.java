package com.sox.users.utility;

public class SOXConstants {

    public static final String NAMESPACE = "sox-user-api";

    public static final String STATUS_CODE_SUCCESS = "200";
    public static final String STATUS_MSG_SUCCESS = "SUCCESS";

    public static final String STATUS_CODE_NOT_FOUND = "404";
    public static final String STATUS_MSG_NOT_FOUND = "NOT FOUND";

    public static final String STATUS_CODE_APPLICATION_ERROR = "500";
    public static final String STATUS_MSG_APPLICATION_ERROR = "APPLICATION ERROR";

    public static final String STATUS_CODE_VALIDATION_ERROR = "400";
    public static final String STATUS_MSG_VALIDATION_ERROR = "VALIDATION ERROR";

    public static final String STATUS_CODE_ERROR = "000";
    public static final String STATUS_MSG_ERROR = "ERROR";

    public static final String STATUS_CODE_UNAUTHORIZED = "401";
    public static final String STATUS_MSG_UNAUTHORIZED = "ACCESS DENIED";

    public static final String DEFAULT_APP_CODE = "0000";
    public static final String DEFAULT_APP_NAME = "Default";
    public static final String DEFAULT_LOC_CODE = "0000";
    public static final String DEFAULT_LOC_NAME = "Default";


    private SOXConstants(){}

}
