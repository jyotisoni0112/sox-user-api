package com.sox.users.repository;

import com.sox.users.entity.UserRoleDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRoleDetailsRepository extends JpaRepository<UserRoleDetails, Integer> {

    List<UserRoleDetails> findByUserId(String userId);
    List<UserRoleDetails> findByUserIdAndActive(String userId, boolean active);
    List<UserRoleDetails> findByRoleIdIn(int[] roleList);
    List<UserRoleDetails> findByAppId(String appId);
    List<UserRoleDetails> findByRoleId(int roleId);
    List<UserRoleDetails> findByLocId(String locId);

    @Query(value = "select * from sox_user_role_details where app_id = ?1 and active = true and (role_id = 12 or role_id = 13 or role_id = 23)", nativeQuery = true)
    List<UserRoleDetails> findActiveContactsForDashboard(String appId);

    @Query(value = "select * from sox_user_role_details where active = true and role_id in(9, 10, 14 ,21, 22)", nativeQuery = true)
    List<UserRoleDetails> findActiveDashboardSOXPMO();

    @Query(value = "select * from sox_user_role_details where active = true and role_id in(9, 10, 14 ,21)", nativeQuery = true)
    List<UserRoleDetails> findActiveDashboardAppSOXPMO();

    @Query(value = "select * from sox_user_role_details where active = true and role_id in(9, 10, 22)", nativeQuery = true)
    List<UserRoleDetails> findActiveDashboardControlSOXPMO();

}
