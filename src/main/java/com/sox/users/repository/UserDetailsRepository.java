package com.sox.users.repository;

import com.sox.users.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDetailsRepository extends JpaRepository<UserDetails, String> {

    List<UserDetails> findByFirstName(String firstName);
    List<UserDetails> findByLastName(String lastName);
    List<UserDetails> findByEmail(String email);
    List<UserDetails> findByUserId(String userId);
}
