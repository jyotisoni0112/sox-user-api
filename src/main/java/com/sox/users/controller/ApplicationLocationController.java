package com.sox.users.controller;

import com.sox.users.entity.ApplicationEntity;
import com.sox.users.exception.SOXException;
import com.sox.users.payload.IdentifierResponse;
import com.sox.users.payload.SOXResponse;
import com.sox.users.service.ApplicationLocationService;
import com.sox.users.utility.SOXConstants;
import com.sox.users.vo.ErrorDetails;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Log4j2
@RequestMapping("/utility")
public class ApplicationLocationController {

    private String className = ApplicationLocationController.class.getName();

    @Autowired
    private ApplicationLocationService applicationLocationService;

    @GetMapping("/app-list")
    public SOXResponse<List<IdentifierResponse>> getApplicationList(){
        String methodName = "getApplicationList";
        SOXResponse<List<IdentifierResponse>> soxResponse = new SOXResponse<>();
        try{
            List<IdentifierResponse> applicationList = applicationLocationService.getApplicationList();
            soxResponse.setData(applicationList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/loc-list")
    public SOXResponse<List<IdentifierResponse>> getLocationList(){
        String methodName = "getLocationList";
        SOXResponse<List<IdentifierResponse>> soxResponse = new SOXResponse<>();
        try{
            List<IdentifierResponse> applicationList = applicationLocationService.getLocationList();
            soxResponse.setData(applicationList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @PostMapping("/add-dashboard-app")
    public SOXResponse<ApplicationEntity> createDashboardApplication(@RequestBody String req){
        return null;
    }

    @PostMapping("/add-uar-app")
    public SOXResponse<ApplicationEntity> createUARApplication(@RequestBody String req){
        return null;
    }

    @GetMapping("/dashboard-app")
    public SOXResponse<List<ApplicationEntity>> getDashboardApps(){
        String methodName = "getDashboardApps";
        SOXResponse<List<ApplicationEntity>> soxResponse = new SOXResponse<>();
        try{
            List<ApplicationEntity> applicationList = applicationLocationService.getDashboardApps();
            soxResponse.setData(applicationList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }
}
