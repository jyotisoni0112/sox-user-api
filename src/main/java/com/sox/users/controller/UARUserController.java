package com.sox.users.controller;

import com.sox.users.entity.UserRoleDetails;
import com.sox.users.exception.SOXException;
import com.sox.users.payload.NewUserRequest;
import com.sox.users.payload.NewUserRoleRequest;
import com.sox.users.payload.SOXResponse;
import com.sox.users.payload.UserResponse;
import com.sox.users.service.AddUserService;
import com.sox.users.utility.RoleUtility;
import com.sox.users.utility.SOXConstants;
import com.sox.users.vo.ErrorDetails;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/uar")
@Log4j2
public class UARUserController {

    private static final Logger logger = LoggerFactory.getLogger(UARUserController.class.getName());
    private static final String className  = UARUserController.class.getName();

    @Autowired
    private AddUserService addUserService;

    @PostMapping("/add-reviewer")
    public SOXResponse<List<UserResponse>> addUserForUAR(@RequestBody NewUserRequest request, Principal principal){
        String methodName = "addUserForUAR";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            for(NewUserRoleRequest role: request.getRoleList()){
                if(!(role.getRoleId() == RoleUtility.ROLE_BUSINESS_POC_1 || role.getRoleId() == RoleUtility.ROLE_BUSINESS_POC_2 || role.getRoleId() == RoleUtility.ROLE_BUSINESS_POC_3
                        || role.getRoleId() == RoleUtility.ROLE_BUSINESS_B_POC_1 || role.getRoleId() == RoleUtility.ROLE_BUSINESS_B_POC_2 || role.getRoleId() == RoleUtility.ROLE_BUSINESS_B_POC_3)){
                    throw new SOXException("Tried to add unauthorized role. "+role.getRoleId(), SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName );
                }
            }
            logger.info("New User Request {}",request);
            List<UserResponse> userResponse = addUserService.addSOXUser(request, principal.getName().toLowerCase());
            soxResponse.setData(userResponse);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

}
