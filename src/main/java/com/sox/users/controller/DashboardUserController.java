package com.sox.users.controller;

import com.sox.users.exception.SOXException;
import com.sox.users.payload.NewUserRequest;
import com.sox.users.payload.NewUserRoleRequest;
import com.sox.users.payload.SOXResponse;
import com.sox.users.payload.UserResponse;
import com.sox.users.service.AddUserService;
import com.sox.users.service.DashboardUserService;
import com.sox.users.utility.RoleUtility;
import com.sox.users.utility.SOXConstants;
import com.sox.users.vo.ErrorDetails;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/dashboard")
@Log4j2
public class DashboardUserController {

    private static final String className = DashboardUserController.class.getName();

    @Autowired
    private AddUserService addUserService;

    @Autowired
    private DashboardUserService dashboardUserService;

    @PostMapping("/add-user")
    public SOXResponse<List<UserResponse>> addNewDashboardUser(@RequestBody NewUserRequest request, Principal principal){
        String methodName = "addNewDashboardUser";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try {
            for(NewUserRoleRequest role: request.getRoleList()){
                if(!(role.getRoleId() == RoleUtility.ROLE_IT_OWNER || role.getRoleId() == RoleUtility.ROLE_BUSINESS_OWNER ||
                        role.getRoleId() == RoleUtility.ROLE_GCA_USERS )){
                    throw new SOXException("Access Denied ", SOXConstants.STATUS_CODE_UNAUTHORIZED, SOXConstants.STATUS_MSG_UNAUTHORIZED, className, methodName);
                }
            }
            List<UserResponse> userResponse = addUserService.addSOXUser(request, principal.getName().toLowerCase());
            soxResponse.setData(userResponse);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/dashboard-app-contact/{appId}")
    public SOXResponse<List<UserResponse>> fetchDashboardApplicationContact(@PathVariable String appId){
        String methodName = "fetchDashboardApplicationContact";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            List<UserResponse> userList = dashboardUserService.getDashboardApplicationContact(appId);
            soxResponse.setData(userList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/dashboard-pmo")
    public SOXResponse<List<UserResponse>> fetchDashboardSOXPMOs(){
        String methodName = "fetchDashboardSOXPMOs";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            List<UserResponse> userList = dashboardUserService.getDashboardSOXPMOs();
            soxResponse.setData(userList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/dashboard-app-pmo")
    public SOXResponse<List<UserResponse>> fetchDashboardApplicationSOXPMOs(){
        String methodName = "fetchDashboardApplicationSOXPMOs";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            List<UserResponse> userList = dashboardUserService.getDashboardAppSOXPMOs();
            soxResponse.setData(userList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/dashboard-control-pmo")
    public SOXResponse<List<UserResponse>> fetchDashboardControlSOXPMOs(){
        String methodName = "fetchDashboardControlSOXPMOs";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            List<UserResponse> userList = dashboardUserService.getDashboardControlSOXPMOs();
            soxResponse.setData(userList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/toggle-role-status/{userRoleId}")
    public SOXResponse<Boolean> toggleUserStatus(@PathVariable int userRoleId, Principal principal){
        String methodName = "toggleUserStatus";
        SOXResponse<Boolean> soxResponse = new SOXResponse<>();
        try{
            boolean success =dashboardUserService.toggleRoleStatus(userRoleId, principal.getName().toLowerCase());
            soxResponse.setData(success);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

}
