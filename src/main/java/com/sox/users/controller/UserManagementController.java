package com.sox.users.controller;

import com.sox.users.exception.SOXException;
import com.sox.users.payload.NewUserRequest;
import com.sox.users.payload.SOXResponse;
import com.sox.users.payload.UserResponse;
import com.sox.users.service.AddUserService;
import com.sox.users.service.UserManagementService;
import com.sox.users.utility.SOXConstants;
import com.sox.users.vo.ErrorDetails;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/sox")
@Log4j2
public class UserManagementController {

    private static final Logger logger = LoggerFactory.getLogger(UserManagementController.class.getName());
    private static final String className = "UserManagementController";

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private AddUserService addUserService;

    @GetMapping("/user-details/{userId}")
    public SOXResponse<List<UserResponse>> getUserDetailsByUserId(@PathVariable String userId){
        String methodName = "getUserDetails";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        soxResponse.setNameSpace(SOXConstants.NAMESPACE);
        try{
            if(userId.isEmpty()){
                throw new SOXException("user_id is invalid",SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            }
            List<UserResponse> userResponse = userManagementService.getUserDetails(userId.toLowerCase());
            soxResponse.setData(userResponse);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                                        ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                        , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @PostMapping("/add-user")
    public SOXResponse<List<UserResponse>> addSOXUser(Principal principal, @RequestBody NewUserRequest userRequest){
        String methodName = "addSOXUser";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try {
            if(userRequest == null)
                throw new SOXException("Request Payload is empty", SOXConstants.STATUS_CODE_VALIDATION_ERROR,
                        SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            if(userRequest.getUserId().isEmpty() || userRequest.getFirstName().isEmpty() || userRequest.getLastName().isEmpty() || userRequest.getEmail().isEmpty())
                throw new SOXException("Request Payload is invalid", SOXConstants.STATUS_CODE_VALIDATION_ERROR,
                        SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            if(userRequest.getUserId().equalsIgnoreCase(principal.getName()))
                throw new SOXException("Cannot add role for self.", SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR,
                        className, methodName);

            List<UserResponse> response = addUserService.addSOXUser(userRequest, principal.getName().toLowerCase());
            soxResponse.setData(response);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @PostMapping("/delete-user/{userId}")
    public SOXResponse<Boolean> deleteSOXUser(@PathVariable String userId, Principal principal){
        String methodName = "deleteSOXUser";
        SOXResponse<Boolean> soxResponse = new SOXResponse<>();
        boolean success;
        try{
            success = userManagementService.deleteUser(userId, principal.getName().toLowerCase());
            soxResponse.setData(success);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/get-logged-in-user-details")
    public SOXResponse<List<UserResponse>> getLoggedInUserDetails(){
        String methodName = "getLoggedInUserDetails";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            List<UserResponse> loggedInUserResponse;
            if(userId.isEmpty()){
                throw new SOXException("Empty User name",SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            }
            loggedInUserResponse = userManagementService.getUserDetails(userId);
            soxResponse.setData(loggedInUserResponse);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/user-by-module/{module}")
    public SOXResponse<List<UserResponse>> getUsersByModule(@PathVariable String module){
        String methodName = "getUsersByModule";
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            logger.info("Module : {}",module);
            if(!("DASHBOARD".equalsIgnoreCase(module) || "UAR".equalsIgnoreCase(module) || "MJE".equalsIgnoreCase(module)
                    || "PAC".equalsIgnoreCase(module) || "USER_STATUS".equalsIgnoreCase(module) || "ADMIN".equalsIgnoreCase(module))){
                throw new SOXException("Invalid module name", SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            }
            List<UserResponse> userRoleDetails = userManagementService.findUsersByModuleAccess(module);
            soxResponse.setData(userRoleDetails);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/search-user/{key}/{value}")
    public SOXResponse<List<UserResponse>> getUsers(@PathVariable String key,@PathVariable String value){
        String methodName = "getUsersByApplication";
        log.info("Searching users with key {} and value {}", key, value);
        SOXResponse<List<UserResponse>> soxResponse = new SOXResponse<>();
        try{
            if(key.isEmpty() || value.isEmpty())
                throw new SOXException("Invalid key value in URL.", SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR
                            , className, methodName);
            List<UserResponse> userList;
            if("FIRST_NAME".equalsIgnoreCase(key) || "LAST_NAME".equalsIgnoreCase(key) ||
                    "EMAIL".equalsIgnoreCase(key) || "USER_ID".equalsIgnoreCase(key))
                userList = userManagementService.findUsersByPersonalDetails(key, value);
            else
                userList = userManagementService.getUsers(key, value);
            soxResponse.setData(userList);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/toggle-user/{userRoleId}")
    public SOXResponse<UserResponse> toggleUserRoleStatus(@PathVariable Integer userRoleId){
        String methodName = "toggleUserRoleStatus";
        SOXResponse<UserResponse> soxResponse = new SOXResponse<>();
        try{
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            UserResponse roleDetails;
            if(userId.isEmpty()){
                throw new SOXException("Empty User name",SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            }
            roleDetails = userManagementService.toggleUserRoleStatus(userRoleId, userId);
            soxResponse.setData(roleDetails);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
            log.info(soxResponse);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }





}
