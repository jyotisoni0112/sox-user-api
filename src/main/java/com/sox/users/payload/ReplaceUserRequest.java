package com.sox.users.payload;

import lombok.Data;

@Data
public class ReplaceUserRequest {

    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String oldUserId;

}
