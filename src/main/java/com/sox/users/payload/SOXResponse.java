package com.sox.users.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sox.users.utility.SOXConstants;
import com.sox.users.vo.ErrorDetails;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SOXResponse<T> {

    @JsonProperty("namespace")
    private String nameSpace = SOXConstants.NAMESPACE;
    @JsonProperty("data")
    private T data;
    @JsonProperty("status_code")
    private String statusCode = SOXConstants.STATUS_CODE_ERROR;
    @JsonProperty("status_msg")
    private String statusMsg = SOXConstants.STATUS_MSG_ERROR;
    @JsonProperty("error_details")
    private ErrorDetails errorDetails;
    @JsonProperty("logging_id")
    private String loggingId;

}
