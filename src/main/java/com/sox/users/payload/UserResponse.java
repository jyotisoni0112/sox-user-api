package com.sox.users.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

    private Integer userRoleId;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private int roleId;
    private String roleName;
    private String appId;
    private String appName;
    private String locId;
    private String locName;
    private boolean active;

}
