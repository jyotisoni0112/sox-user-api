package com.sox.users.payload;

import lombok.Data;

import java.util.List;

@Data
public class NewUserRequest {

    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private List<NewUserRoleRequest> roleList;

}
