package com.sox.users.payload;

import lombok.Data;

@Data
public class NewUserRoleRequest {

    private int roleId;
    private String locId;
    private String appId;

}
