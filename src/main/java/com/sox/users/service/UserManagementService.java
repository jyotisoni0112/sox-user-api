package com.sox.users.service;

import com.sox.users.entity.*;
import com.sox.users.exception.SOXException;
import com.sox.users.payload.NewUserRequest;
import com.sox.users.payload.NewUserRoleRequest;
import com.sox.users.payload.ReplaceUserRequest;
import com.sox.users.payload.UserResponse;
import com.sox.users.repository.UserDetailsRepository;
import com.sox.users.repository.UserInfoRepository;
import com.sox.users.repository.UserRoleDetailsRepository;
import com.sox.users.utility.RoleUtility;
import com.sox.users.utility.SOXConstants;
import com.sox.users.utility.UtilityMethod;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class UserManagementService {

    private static final Logger logger = LoggerFactory.getLogger(UserManagementService.class.getName());
    private static final String className = "UserManagementService";

    @Autowired
    private UserDetailsRepository userEntityRepository;

    @Autowired
    private UserRoleDetailsRepository userRoleDetailsRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private AddUserService addUserService;

    public List<UserResponse> getUserDetails(String userId) throws SOXException {
        String methodName = "getUserDetails";
        List<UserRoleDetails> roleDetails;
        List<UserResponse> response = new ArrayList<>();
        try{
            roleDetails = userRoleDetailsRepository.findByUserIdAndActive(userId, true);
            if(roleDetails.isEmpty()){
                UserResponse userResponse = new UserResponse();
                Optional<UserInfo> userInfo = userInfoRepository.findById(userId);
                if(userInfo.isPresent()){
                    userResponse.setUserRoleId(0);
                    userResponse.setUserId(userId);
                    userResponse.setFirstName(userInfo.get().getFirstName());
                    userResponse.setLastName(userInfo.get().getLastName());
                    userResponse.setEmail(userInfo.get().getEmail());
                    userResponse.setRoleId(0);
                    userResponse.setRoleName("No_ROLE");
                    userResponse.setAppName("Default");
                    userResponse.setAppId("0000");
                    userResponse.setLocName("Default");
                    userResponse.setLocId("0000");
                    userResponse.setActive(true);
                    response.add(userResponse);
                }else{
                    throw new SOXException("User not Present in System.", SOXConstants.STATUS_CODE_NOT_FOUND,
                            SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
                }
            }else{
                response = UtilityMethod.mapRoleResponseToUserResponse(roleDetails);
            }

        }catch (SOXException exception){
            throw exception;
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return response;
    }


    public boolean deleteUser(String userId, String loggedInUserName) throws SOXException {
        String methodName = "deleteUser";
        try{
            Optional<UserDetails> userDetails = userEntityRepository.findById(userId.toLowerCase());
            if (userDetails.isPresent()){
                List<UserRoleDetails> roleDetailsList = userRoleDetailsRepository.findByUserId(userId.toLowerCase());
                roleDetailsList.forEach( role -> {
                    if(role.isActive()){
                        role.setActive(false);
                        role.setUpdatedBy(loggedInUserName);
                        role.setUpdatedOn(new Date());
                        userRoleDetailsRepository.save(role);
                    }
                });
                userDetails.get().setActive(false);
                userEntityRepository.save(userDetails.get());
            }else {
                throw new SOXException("User not found with userId - "+userId,SOXConstants.STATUS_CODE_NOT_FOUND,SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
            }
        }catch (SOXException exception){
            throw exception;
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return true;
    }

    @PostMapping("/replace-user")
    public List<UserResponse> replaceUser(@RequestBody ReplaceUserRequest request, Principal principal) throws SOXException {
        String methodName = "replaceUser";
        List<UserResponse> replacedUser;
        try{
            List<UserRoleDetails> rolesToBeAdded = userRoleDetailsRepository.findByUserIdAndActive(request.getOldUserId(), true);
            if(!rolesToBeAdded.isEmpty()){
                NewUserRequest newUserRequest = new NewUserRequest();
                newUserRequest.setUserId(request.getUserId());
                newUserRequest.setFirstName(request.getFirstName());
                newUserRequest.setLastName(request.getLastName());
                newUserRequest.setEmail(request.getEmail());
                List<NewUserRoleRequest> roleRequestList = new ArrayList<>();
                rolesToBeAdded.forEach(role->{
                    NewUserRoleRequest roleRequest = new NewUserRoleRequest();
                    roleRequest.setRoleId(role.getRoleId());
                    roleRequest.setAppId(role.getAppId());
                    roleRequest.setLocId(role.getLocId());
                    roleRequestList.add(roleRequest);
                });
                newUserRequest.setRoleList(roleRequestList);
                deleteUser(request.getUserId().toLowerCase(), principal.getName().toLowerCase());
                replacedUser = addUserService.addSOXUser(newUserRequest, principal.getName().toLowerCase());

            }else{
                logger.error("No roles found for user {} {} - {}",request.getFirstName(), request.getLastName(),request.getUserId());
                throw new SOXException("No Roles found for the user "+request.getFirstName()+ request.getLastName()+" - "+request.getUserId()
                        , SOXConstants.STATUS_CODE_NOT_FOUND, SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
            }
        }catch(SOXException exception){
            throw exception;
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return replacedUser;
    }

    public List<UserResponse> getUsers(String key, String value) throws SOXException {
        String methodName = "getUsersByApplication";
        List<UserRoleDetails> userRoleDetailsList;
        List<UserResponse> response;
        try{
            if("APP".equalsIgnoreCase(key)){
                userRoleDetailsList = userRoleDetailsRepository.findByAppId(value);
            }else if("ROLE".equalsIgnoreCase(key)){
                userRoleDetailsList = userRoleDetailsRepository.findByRoleId(Integer.parseInt(value));
            }else if("LOC".equalsIgnoreCase(key)){
                userRoleDetailsList = userRoleDetailsRepository.findByLocId(value);
            }else if("USER_ID".equalsIgnoreCase(key)){
                userRoleDetailsList = userRoleDetailsRepository.findByUserId(value);
            }else{
                userRoleDetailsList = new ArrayList<>();
            }
            response = UtilityMethod.mapRoleResponseToUserResponse(userRoleDetailsList);

        }catch (Exception e){
            log.error(e);
            throw new SOXException("Exception while fetching application contacts.", SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return response;
    }

    public List<UserResponse> findUsersByPersonalDetails(String key, String value) throws SOXException{
        String methodName = "getUsersByApplication";
        List<UserRoleDetails> userRoleDetailsList = new ArrayList<>();
        List<UserResponse> response;
        try{
            List<UserDetails> user = new ArrayList<>();
            if("FIRST_NAME".equalsIgnoreCase(key)){
                user = userEntityRepository.findByFirstName(UtilityMethod.capitalize(value));
            }else if("USER_ID".equalsIgnoreCase(key)){
                user = userEntityRepository.findByUserId(value.toLowerCase());
            }else if("LAST_NAME".equalsIgnoreCase(key)){
                user = userEntityRepository.findByLastName(UtilityMethod.capitalize(value));
            }else if("EMAIL".equalsIgnoreCase(key)){
                user = userEntityRepository.findByEmail(value.toLowerCase());
            }else{
                userRoleDetailsList = new ArrayList<>();
            }

            if(!user.isEmpty()){
                for(UserDetails role: user){
                    List<UserRoleDetails> userRoleDetails = userRoleDetailsRepository.findByUserId(role.getUserId());
                    if(userRoleDetails.isEmpty()){
                        Optional<UserInfo> userInfo = userInfoRepository.findById(role.getUserId());
                        if(userInfo.isPresent()){
                            UserRoleDetails details = new UserRoleDetails();
                            details.setUserRoleId(0);
                            details.setUserId(role.getUserId());
                            details.setActive(role.isActive());
                            details.setRoleId(0);
                            details.setAppId(SOXConstants.DEFAULT_APP_CODE);
                            details.setLocId(SOXConstants.DEFAULT_LOC_CODE);
                            LocationEntity locationEntity = new LocationEntity(SOXConstants.DEFAULT_LOC_CODE, SOXConstants.DEFAULT_LOC_NAME);
                            ApplicationEntity applicationEntity = new ApplicationEntity();
                            applicationEntity.setAppId(SOXConstants.DEFAULT_APP_CODE);
                            applicationEntity.setAppName(SOXConstants.DEFAULT_APP_NAME);
                            details.setLocation(locationEntity);
                            details.setApplication(applicationEntity);
                            UserInfo users = new UserInfo(role.getUserId(), role.getFirstName(),
                                    role.getLastName(), role.getEmail());
                            details.setUserInfo(users);
                            userRoleDetailsList.add(details);
                        }else{
                            throw new SOXException("",SOXConstants.STATUS_CODE_NOT_FOUND, SOXConstants.STATUS_MSG_NOT_FOUND
                                    ,className, methodName);
                        }
                    }else{
                        userRoleDetailsList.addAll(userRoleDetails);
                    }

                }
                response = UtilityMethod.mapRoleResponseToUserResponse(userRoleDetailsList);
            }else{
                response = new ArrayList<>();
            }


        }catch (Exception e){
            log.error(e);
            throw new SOXException("Exception while fetching application contacts.", SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return response;
    }

    public List<UserResponse> findUsersByModuleAccess(String module) throws SOXException {
        String methodName = "findUsersByModuleAccess";
        List<UserRoleDetails> userList;
        List<UserResponse> response;
        try{
            int[] roleCodes = RoleUtility.roleMapping(module.toUpperCase());
            logger.info("Role List : {}", roleCodes);
            userList = userRoleDetailsRepository.findByRoleIdIn(roleCodes);
            response = UtilityMethod.mapRoleResponseToUserResponse(userList);
        }catch (Exception e){
            logger.error("Exception while fetching users by module. ", e);
            throw new SOXException("Exception while fetching users by module.", SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return response;
    }

    public UserResponse toggleUserRoleStatus(Integer userRoleId, String userId) throws SOXException {
        String methodName = "toggleUserRoleStatus";
        UserResponse userResponse;
        try{
            Optional<UserRoleDetails> userRoleDetail = userRoleDetailsRepository.findById(userRoleId);
            if(userRoleDetail.isPresent()){
                userRoleDetail.get().setActive(!userRoleDetail.get().isActive());
                userRoleDetail.get().setUpdatedOn(new Date());
                userRoleDetail.get().setUpdatedBy(userId);
                List<UserRoleDetails> roleList = new ArrayList<>();
                roleList.add(userRoleDetailsRepository.save(userRoleDetail.get()));
                userResponse = UtilityMethod.mapRoleResponseToUserResponse(roleList).get(0);
            }else{
                log.error("Role not found with primary key - "+userRoleId);
                throw new SOXException("Role not found.", SOXConstants.STATUS_CODE_NOT_FOUND,
                        SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
            }
        }catch (SOXException exception){
            throw exception;
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return userResponse;
    }
}
