package com.sox.users.service;

import com.sox.users.entity.UserDetails;
import com.sox.users.entity.UserInfo;
import com.sox.users.entity.UserRoleDetails;
import com.sox.users.exception.SOXException;
import com.sox.users.payload.NewUserRequest;
import com.sox.users.payload.NewUserRoleRequest;
import com.sox.users.payload.UserResponse;
import com.sox.users.repository.UserDetailsRepository;
import com.sox.users.repository.UserInfoRepository;
import com.sox.users.repository.UserRoleDetailsRepository;
import com.sox.users.utility.SOXConstants;
import com.sox.users.utility.UtilityMethod;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class AddUserService {

    private static final String className = "AddUserService";

    @Autowired
    private UserDetailsRepository userEntityRepository;

    @Autowired
    private UserRoleDetailsRepository userRoleDetailsRepository;

    @Autowired
    private UserInfoRepository usersRepository;

    @Transactional
    public List<UserResponse> addSOXUser(NewUserRequest request, String loggedInUserId) throws SOXException {
        String methodName = "addSOXUser";
        List<UserRoleDetails> userList;
        List<UserResponse> response;
        try{
            Optional<UserDetails> userPresent = userEntityRepository.findById(request.getUserId());
            if(userPresent.isPresent()){
                if(!userPresent.get().isActive()){
                    log.info("Activating User {} {} - {}", userPresent.get().getFirstName(),userPresent.get().getLastName(),userPresent.get().getUserId());
                    activateUser(userPresent.get(), loggedInUserId);
                }
                List<UserRoleDetails> roleDetailsList = userRoleDetailsRepository.findByUserId(request.getUserId().toLowerCase());
                userList = updateUserWithNewRoles(roleDetailsList, request.getRoleList(), request.getUserId().toLowerCase(), loggedInUserId);
            }else{
                userList = addNewUserDetailsAndRoles(request, loggedInUserId);
            }
            response = UtilityMethod.mapRoleResponseToUserResponse(userList);
        }catch (SOXException exception){
            throw exception;
        } catch(Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return response;
    }

    private List<UserRoleDetails> addNewUserDetailsAndRoles(NewUserRequest request, String loggedInUserId) throws SOXException {
        String methodName = "addNewUserDetailsAndRoles";
        List<UserRoleDetails> response;
        try{
            UserDetails addUser = new UserDetails();
            addUser.setUserId(request.getUserId().toLowerCase());
            addUser.setFirstName(UtilityMethod.capitalize(request.getFirstName()));
            addUser.setLastName(UtilityMethod.capitalize(request.getLastName()));
            addUser.setEmail(request.getEmail().toLowerCase());
            addUser.setActive(true);
            addUser.setCreatedBy(loggedInUserId);
            addUser.setCreatedOn(new Date());
            addUser.setUpdatedBy(loggedInUserId);
            addUser.setUpdatedOn(new Date());
            UserDetails userDetails = userEntityRepository.save(addUser);
            log.info("Add new user to SOX System. - {}", userDetails);
            List<UserRoleDetails> roleList = new ArrayList<>();
            request.getRoleList().forEach(role -> {
                UserRoleDetails roleDetails = new UserRoleDetails();
                roleDetails.setUserId(request.getUserId().toLowerCase());
                roleDetails.setRoleId(role.getRoleId());
                if(role.getRoleId() == 9 || role.getRoleId() == 10){
                    roleDetails.setAppId("0000");
                    roleDetails.setLocId("0000");
                }else {
                    roleDetails.setAppId(role.getAppId());
                    roleDetails.setLocId(role.getLocId());
                }
                roleDetails.setActive(true);
                roleDetails.setCreatedBy(loggedInUserId);
                roleDetails.setCreatedOn(new Date());
                roleDetails.setUpdatedBy(loggedInUserId);
                roleDetails.setUpdatedOn(new Date());
                log.info("Adding new Role for user. Role details - {}", roleDetails);
                roleList.add(roleDetails);
            });
            UserInfo user = new UserInfo(userDetails.getUserId(), userDetails.getFirstName(), userDetails.getLastName(), userDetails.getEmail());
            usersRepository.save(user);
            userRoleDetailsRepository.saveAll(roleList);
            response = userRoleDetailsRepository.findByUserId(user.getUserId());
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return  response;
    }

    private void activateUser(UserDetails user, String loggedInUserId) throws SOXException {
        String methodName = "activateUser";
        try{
            user.setActive(true);
            user.setUpdatedBy(loggedInUserId);
            user.setUpdatedOn(new Date());
            userEntityRepository.save(user);
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }

    }

    private List<UserRoleDetails> updateUserWithNewRoles(List<UserRoleDetails> presentRoleList, List<NewUserRoleRequest> rolesToBeAdded, String userId, String loggedInUserId) throws SOXException {
        String methodName = "updateUserWithNewRoles";
        List<UserRoleDetails> response = new ArrayList<>();
        try{
            rolesToBeAdded.forEach( roleToBeAdded -> {
                boolean rolePresent = false;
                for(UserRoleDetails role : presentRoleList){
                    if((roleToBeAdded.getRoleId() == role.getRoleId()) && (roleToBeAdded.getAppId().equals(role.getAppId()) &&
                            (role.getLocId().equals(roleToBeAdded.getLocId())))){
                        if(!role.isActive()){
                            role.setActive(true);
                            role.setUpdatedBy(loggedInUserId);
                            role.setUpdatedOn(new Date());
                            UserRoleDetails updatedRole = userRoleDetailsRepository.save(role);
                            log.info("Activating role for user. {}", role);
                            response.add(updatedRole);
                        }else{
                            log.info("Role already present. {}", role);
                            response.add(role);
                        }
                        rolePresent = true;
                        break;
                    }
                }
                if(!rolePresent){
                    UserRoleDetails addRole = new UserRoleDetails();
                    addRole.setUserId(userId);
                    addRole.setRoleId(roleToBeAdded.getRoleId());
                    addRole.setAppId(roleToBeAdded.getAppId());
                    addRole.setLocId(roleToBeAdded.getLocId());
                    addRole.setCreatedBy(loggedInUserId);
                    addRole.setActive(true);
                    addRole.setCreatedOn(new Date());
                    addRole.setUpdatedBy(loggedInUserId);
                    addRole.setUpdatedOn(new Date());
                    UserRoleDetails addedRole = userRoleDetailsRepository.save(addRole);
                    response.add(addedRole);
                }
            });
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return response;
    }
}
