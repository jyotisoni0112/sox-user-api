package com.sox.users.service;

import com.sox.users.entity.ApplicationEntity;
import com.sox.users.exception.SOXException;
import com.sox.users.payload.IdentifierResponse;
import com.sox.users.repository.ApplicationRepository;
import com.sox.users.repository.LocationRepository;
import com.sox.users.utility.SOXConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j2
public class ApplicationLocationService {

    private String className = ApplicationLocationService.class.getName();

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private LocationRepository locationRepository;

    public List<IdentifierResponse> getApplicationList() throws SOXException {
        String methodName = "getApplicationList";
        List<IdentifierResponse> applicationList;
        try{
            applicationList = applicationRepository.findAll()
                    .stream()
                    .map(app -> new IdentifierResponse(app.getAppName(), app.getAppId()))
                    .collect(Collectors.toList());
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return applicationList;
    }

    public List<IdentifierResponse> getLocationList() throws SOXException {
        String methodName = "getLocationList";
        List<IdentifierResponse> locationList;
        try{
            locationList = locationRepository.findAll()
                    .stream()
                    .map(loc -> new IdentifierResponse(loc.getLocName(), loc.getLocId()))
                    .collect(Collectors.toList());
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return locationList;
    }

    public List<ApplicationEntity> getDashboardApps() throws SOXException {
        String methodName = "getDashboardApps";
        List<ApplicationEntity> applicationEntities;
        try{
                applicationEntities = applicationRepository.findAll();
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return applicationEntities;
    }
}
