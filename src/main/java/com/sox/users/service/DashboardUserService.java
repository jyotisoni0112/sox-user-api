package com.sox.users.service;

import com.sox.users.entity.UserRoleDetails;
import com.sox.users.exception.SOXException;
import com.sox.users.payload.UserResponse;
import com.sox.users.repository.UserRoleDetailsRepository;
import com.sox.users.utility.SOXConstants;
import com.sox.users.utility.UtilityMethod;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class DashboardUserService {

    private static final Logger logger = LoggerFactory.getLogger(DashboardUserService.class.getName());
    private static final String className = DashboardUserService.class.getName();

    @Autowired
    private UserRoleDetailsRepository roleDetailsRepository;

    public List<UserResponse> getDashboardSOXPMOs() throws SOXException {
        String methodName = "getDashboardSOXPMOs";
        List<UserRoleDetails> dashboardPMOList;
        List<UserResponse> response;
        try{
            dashboardPMOList = roleDetailsRepository.findActiveDashboardSOXPMO();
            response = UtilityMethod.mapRoleResponseToUserResponse(dashboardPMOList);
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR,className, methodName);
        }
        return response;
    }

    public List<UserResponse> getDashboardAppSOXPMOs() throws SOXException {
        String methodName = "getDashboardAppSOXPMOs";
        List<UserRoleDetails> dashboardPMOList;
        List<UserResponse> responses;
        try{
            dashboardPMOList = roleDetailsRepository.findActiveDashboardAppSOXPMO();
            responses = UtilityMethod.mapRoleResponseToUserResponse(dashboardPMOList);
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR,className, methodName);
        }
        return responses;
    }

    public List<UserResponse> getDashboardControlSOXPMOs() throws SOXException {
        String methodName = "getDashboardControlSOXPMOs";
        List<UserRoleDetails> dashboardPMOList;
        List<UserResponse> response;
        try{
            dashboardPMOList = roleDetailsRepository.findActiveDashboardControlSOXPMO();
            response = UtilityMethod.mapRoleResponseToUserResponse(dashboardPMOList);
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR,className, methodName);
        }
        return response;
    }

    public List<UserResponse> getDashboardApplicationContact(String appId) throws SOXException {
        String methodName = "getDashboardApplicationContact";
        List<UserRoleDetails> dashboardPMOList;
        List<UserResponse> response;
        try{
            dashboardPMOList = roleDetailsRepository.findActiveContactsForDashboard(appId);
            response = UtilityMethod.mapRoleResponseToUserResponse(dashboardPMOList);
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR,className, methodName);
        }
        return  response;
    }

    public boolean toggleRoleStatus(int userRoleId, String loggedInUserId) throws SOXException {
        String methodName = "toggleRoleStatus";
        try{
            Optional<UserRoleDetails> role = roleDetailsRepository.findById(userRoleId);
            if(role.isPresent()){
                role.get().setActive(!role.get().isActive());
                role.get().setUpdatedBy(loggedInUserId);
                role.get().setUpdatedOn(new Date());
                roleDetailsRepository.save(role.get());
            }else{
                logger.info("User not found with primary Key, {}", userRoleId);
                throw new SOXException("User not found with primary Key "+userRoleId, SOXConstants.STATUS_CODE_NOT_FOUND,
                        SOXConstants.STATUS_MSG_NOT_FOUND,className, methodName);
            }
        }catch (SOXException exception){
            throw exception;
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR,className, methodName);
        }
        return true;
    }

}
