package com.sox.users.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sox_user_role_details")
@Data
public class UserRoleDetails {

    @Id
    @Column(name = "user_role_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userRoleId;
    @Column(name = "user_id")
    private String userId;
    @Column(name = "role_id")
    private int roleId;
    @Column(name = "app_id")
    private String appId;
    @Column(name = "loc_id")
    private String locId;
    @Column(name = "active")
    private boolean active;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_on")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdOn;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_on")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updatedOn;
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserInfo userInfo;
    @OneToOne
    @JoinColumn(name = "loc_id", referencedColumnName = "loc_id", insertable = false, updatable = false)
    private LocationEntity location;
    @OneToOne
    @JoinColumn(name = "app_id", referencedColumnName = "app_id", insertable = false, updatable = false)
    private ApplicationEntity application;

}
