package com.sox.users.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "sox_location_details")
@NoArgsConstructor
@AllArgsConstructor
public class LocationEntity {

    @Id
    @Column(name = "loc_id")
    private String locId;
    @Column(name = "location_name")
    private String locName;

}
