package com.sox.users.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "sox_application_details")
@Data
public class ApplicationEntity {

    @Id
    @Column(name = "app_id")
    private String appId;
    @Column(name = "app_name")
    @NotBlank(message = "Application or Control name cannot be empty")
    private String appName;
    @Column(name = "app_label")
    @NotBlank(message = "Application or Control Flag required")
    private String appLabel;
    @Column(name = "sox_active")
    private boolean soxActive;
    @Column(name = "sod_active")
    private boolean sodActive;
    @Column(name = "dashboard_active")
    private boolean dashboardActive;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_on")
    private Date createdOn;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_on")
    private Date updatedOn;

}
