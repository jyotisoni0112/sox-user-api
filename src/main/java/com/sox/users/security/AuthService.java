package com.sox.users.security;

import com.sox.users.entity.UserDetails;
import com.sox.users.entity.UserRoleDetails;
import com.sox.users.repository.UserDetailsRepository;
import com.sox.users.repository.UserRoleDetailsRepository;
import com.sox.users.utility.RoleUtility;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@Log4j2
public class AuthService {

    @Autowired
    private UserRoleDetailsRepository userRoleDetailsRepository;

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    public User getUserByUsername(String username){
        Set<GrantedAuthority> authorities = new HashSet<>();
        List<UserRoleDetails> roleDetailsList = userRoleDetailsRepository.findByUserIdAndActive(username, true);
        String userName;
        if(roleDetailsList.isEmpty()){
            Optional<UserDetails> userDetails = userDetailsRepository.findById(username);
            if(userDetails.isPresent()){
                userName = userDetails.get().getUserId();
            }else{
                throw new UsernameNotFoundException(username);
            }
        }else{
            roleDetailsList
                    .forEach(role -> authorities.add(new SimpleGrantedAuthority(RoleUtility.getRoleName(role.getRoleId()))));
            userName = roleDetailsList.get(0).getUserId();
        }

        return new User(userName,"password", authorities);
    }
}
