package com.sox.users.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDetails {

    @JsonProperty("error_msg")
    private String errorMsg;
    @JsonProperty("status_code")
    private String statusCode;
    @JsonProperty("status_msg")
    private String statusMsg;
    @JsonProperty("class_name")
    private String className;
    @JsonProperty("method_name")
    private String methodName;
    @JsonProperty("namespace")
    private String nameSpace;

}
